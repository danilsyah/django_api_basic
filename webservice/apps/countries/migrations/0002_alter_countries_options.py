# Generated by Django 3.2 on 2022-05-12 00:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('countries', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='countries',
            options={'managed': False},
        ),
    ]
