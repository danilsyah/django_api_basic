from django.db import models

# Create your models here.
class Countries(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=False, default='')
    capital = models.CharField(max_length=50, blank=False, default='')
    updateAt = models.DateTimeField(auto_now=True, blank=True, null=True)
    createAt = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    
    class Meta:
        managed=False
        db_table = 'countries'
        ordering = ('id',)
    
    def __str__(self):
        return self.name